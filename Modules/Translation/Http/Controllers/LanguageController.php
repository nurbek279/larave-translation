<?php

namespace Modules\Translation\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Translation\Entities\Language;

class LanguageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('translation::index');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return JsonResponse
     */
    public function store(Request $request)
    {
        return successCreated();
    }

    /**
     * Show the specified resource.
     *
     * @return Language
     */
    public function show(Request $request, Language $language)
    {
        return $language;
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, Language $language): JsonResponse
    {
        return successUpdated();
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Language $language): JsonResponse
    {
        return successDeleted();
    }
}
