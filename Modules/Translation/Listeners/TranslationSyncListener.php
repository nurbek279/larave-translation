<?php

namespace Modules\Translation\Listeners;

use Modules\Translation\Events\TranslationChangeEvent;

class TranslationSyncListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @return void
     */
    public function handle(TranslationChangeEvent $event)
    {
        //
    }
}
