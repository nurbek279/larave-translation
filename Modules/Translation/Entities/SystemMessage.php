<?php

namespace Modules\Translation\Entities;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class SystemMessage extends Model
{
    const CATEGORY_BACKEND = 'backend';

    const CATEGORY_FRONT = 'front';

    const CATEGORY_MOBIL = 'mobil';

    protected $fillable = [
        'category',
        'message',
    ];

    public function translations(): HasMany
    {
        return $this->hasMany(SystemMessageTranslation::class, 'message_id');
    }
}
