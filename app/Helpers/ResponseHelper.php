<?php

use Illuminate\Http\JsonResponse;

function successDeleted(): JsonResponse
{
    return response()->json([
        'status' => 'success',
        'message' => 'Deleted',
        'data' => [],
    ]);
}

function successCreated($model): JsonResponse
{
    return response()->json([
        'status' => 'success',
        'message' => 'Created',
        'data' => $model,
    ]);
}

function successUpdated(): JsonResponse
{
    return response()->json([
        'status' => 'success',
        'message' => 'Updated',
        'data' => [],
    ]);
}
