<?php

use Illuminate\Support\Facades\Route;
use Modules\Translation\Http\Controllers\LanguageController;
use Modules\Translation\Http\Controllers\TranslationController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::prefix('v1')->group(function () {
    Route::prefix('admin')->middleware('auth')->group(function () {
        Route::apiResource('languages', LanguageController::class);
    });
    Route::apiResource('languages', LanguageController::class);

    Route::prefix('admin/translations')->middleware('auth')->group(function () {
        Route::get('/', [TranslationController::class, "index"]);
        Route::post('/', [TranslationController::class, "store"]);
    });
});
