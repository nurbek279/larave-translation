<?php

namespace Modules\Translation\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Translation\Http\Repositories\Interfaces\iTranslationInterface;
use Modules\Translation\Http\Repositories\TranslationRepository;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register the service provider.
     */
    public function register(): void
    {
        $this->app->bind(iTranslationInterface::class, TranslationRepository::class);

    }

    /**
     * Get the services provided by the provider.
     */
    public function provides(): array
    {
        return [];
    }
}
