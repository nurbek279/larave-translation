<?php

namespace Modules\Translation\Http\Repositories\Interfaces;

use Illuminate\Http\JsonResponse;
use Modules\Translation\Entities\SystemMessage;
use Modules\Translation\Http\Requests\TranslationStoreRequest;

interface iTranslationInterface
{
    public function store(TranslationStoreRequest $request, SystemMessage $message): JsonResponse;
}
