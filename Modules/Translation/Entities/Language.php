<?php

namespace Modules\Translation\Entities;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    const STATUS_ACTIVE = 1;

    const STATUS_INACTIVE = 0;

    /**
     * @var string<int, string>
     */
    protected $fillable = [
        'name',
        'code',
        'icon_id',
        'status',
    ];

    public static function getLangId(string $lang)
    {
        return self::where('code', $lang)->pluck('id')->first();
    }

    public static function getLangCode(int $id)
    {
        return self::where('id', $id)->pluck('code')->first();
    }
}
