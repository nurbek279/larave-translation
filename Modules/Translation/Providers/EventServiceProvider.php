<?php

namespace Modules\Translation\Providers;

use Illuminate\Support\ServiceProvider;
use Modules\Translation\Events\TranslationChangeEvent;
use Modules\Translation\Listeners\TranslationSyncListener;

class EventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     */
    protected array $listen = [
        TranslationChangeEvent::class => [
            TranslationSyncListener::class,
        ],

    ];

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Get the services provided by the provider.
     */
    public function provides(): array
    {
        return [];
    }
}
