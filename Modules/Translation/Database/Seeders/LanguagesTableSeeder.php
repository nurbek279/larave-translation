<?php

namespace Modules\Translation\Database\Seeders;

use Illuminate\Database\Seeder;
use Modules\Translation\Entities\Language;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $languages = [
            ['name' => 'Uz', 'code' => 'uz', 'status' => 1],
            ['name' => 'Уз', 'code' => 'oz', 'status' => 1],
            ['name' => 'Ру', 'code' => 'ru', 'status' => 1],
            ['name' => 'EN', 'code' => 'en', 'status' => 1],
        ];

        foreach ($languages as $language) {
            Language::query()->create([
                'name' => $language['name'],
                'code' => $language['code'],
                'status' => $language['status'],
            ]);
        }
    }
}
