<?php

namespace Modules\Translation\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Translation\Entities\SystemMessage;
use Modules\Translation\Http\Repositories\Interfaces\iTranslationInterface;
use Modules\Translation\Http\Requests\TranslationStoreRequest;

class TranslationController extends Controller
{
    public function __construct(private readonly iTranslationInterface $translation)
    {
    }

    /**
     * Display a listing of the resource.
     *
     * @return Renderable
     */
    public function index()
    {
        return view('translation::index');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(TranslationStoreRequest $request, SystemMessage $message): JsonResponse
    {
        return $this->translation->store($request, $message);
    }

    /**
     * Show the specified resource.
     *
     * @param  int  $id
     * @return int
     */
    public function show($id)
    {
        return $id;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        return successUpdated();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return JsonResponse
     */
    public function destroy($id)
    {
        return successDeleted();
    }
}
