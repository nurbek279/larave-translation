<?php

namespace Modules\Translation\Http\Repositories;

use Illuminate\Http\JsonResponse;
use Modules\Translation\Entities\SystemMessage;
use Modules\Translation\Http\Repositories\Interfaces\iTranslationInterface;
use Modules\Translation\Http\Requests\TranslationStoreRequest;

class TranslationRepository implements iTranslationInterface
{
    public function store(TranslationStoreRequest $request, SystemMessage $message): JsonResponse
    {
        $model = $message->create($request->all());

        return successCreated($model);
    }
}
